import React from 'react';
import {BrowserRouter as Router,Route,Redirect} from 'react-router-dom';

import './App.css';
import Sidebar from './components/Siderbar/Sidebar';

function App() {
  return (
    <Router>
      <div className="d-flex">
            <Sidebar/>
            <div className="w-100">
              <div id="content">
                 {/* Dentro de este content va todo lo relacido dentro de el cuerpo 
                 ejemplo: Mapas botones internos */}
              </div>
            </div>   
      </div>
    </Router>
  );
}

export default App;
