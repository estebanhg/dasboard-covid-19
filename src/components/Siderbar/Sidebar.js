import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import './Sidebar.css'

export default function Sidebar() {
    
    

    return (
       
            <div id="sidebar-container" className="bg-primary">
                <div className="logo">
                    <h4 className="text-light font-weight-bold">DASBOARD</h4>
                </div>
                <div className="menu">
                    
                         <div>
                            <span className="d-block text-light p-2 font-weight-bold">DATOs</span>
                            <a href="#" className="d-block text-light p-4"><i className="fas fa-home"></i> Inicio</a>
                            <a href="#" className="d-block text-light p-4"><i className="fas fa-user-lock"></i> Administrativo</a>
                             <a href="#" className="d-block text-light p-4"><i className="fas fa-briefcase"></i> Contratistas</a>
                            <a href="#" className="d-block text-light p-4"><i className="fas fa-hard-hat"></i> Empleados</a>
                               
                        </div>
                        
                   
                </div>
            
            </div>
        
    )
}
